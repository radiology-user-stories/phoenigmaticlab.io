| **Introduction** |
| --- |

The following documentation is the user stories prepared for the developer\(s\) to understand the perspective of prospective users as regards the radiology reporting framework.

| **Purpose: ** |
| --- |
| To develop a guideline  to support radiology reporting module in LibreHealth. |

| **Intended Audience:** |
| --- |
| Application Developer\(s\) |

| Project Scope: |
| --- |
| The scope of this document describes what will be provided for during the development of the radiology module to support radiology reporting for LibreHealth. |

| **Items Covered** |
| --- |
| Radiology Reporting |

| **Definitions:** |
| --- |
| The following are the definition of the key concepts used in the user stories developed below. |

:**Report** - This is is the documentation developed by the user after thorough examination of the current study and comparison with other studies conducted on the patient.

**Dictation** - This is the process of documentation using alternative method to keyboard by using 3rd-party speech recognition software integrated with the Radiology Information System.

**Draft** - This is the stage at which a report or a study\(images\) assume when it has been claimed by a particular user. This would prevent other users from reclaiming it.

**Addendum**- This is additional reporting activity conducted on a report that has assumed a ‘Finalized’ status

**Preliminary - **This is is the stage at which a report assumes when awaiting approval from a user who is higher in ranks.

**Finalized-** This is the final stage of reporting. At this stage the report is visible to the ordering physician

**Radiologist-**A personnel trained to offer reporting support to the clinician

**Staff- **A full-fledge radiologist who is authorized to approve report to the ‘finalized’ stage

**Trainee -** A radiologist in training who conducts radiology reporting under the supervision of a staff

**Instance-\(To be added\)**

**Study-**This is a the folder containing the images sent by the Technologist.

**Series-** **\(To be added\)**

| **Use Cases** |
| --- |

**Components of the Radiology Information System**

**Template Generator**
The template generator serves as a repository for templates that can be reused by the user for reporting on different types of study. The sections will contain the following:

* **Indications:** The main reason for the study

* **Date**: This would be retrieved from the system when the particular template is chosen

* **Comparison**: This would contain the previous study that the user wishes to compare the current study with

* **Technique:** This shows the method used by the Technologist and the it would be retrieved from the study when the template is chosen

* **Findings:** Would be recorded by the user throw dictation of typing

* **Impression:** Would contain the speculations made by the user


**Dictation Software:**

This is the separate voice-recognition software that would be integrated with the reporting software for easy documentation. The use would speak to a compatible mouthpiece device to record reports. Apart from documentation, it would be designed to have additional functionalities such as command recognition and execution e.g. erase, remove three words forward, skip to 6 words backwards etc.

**Reporting Software**

This is an editor to be used by the user to perform reporting activities. Templates will be hosted on the Reporting Software and it will be resumed on claiming a new study or updating a previously claimed study.

* **User’s Report**

* The user’s report would be viewed on the reporting software and it will contain a breakdown of studies that are grouped under different categories based on the status of the study. The user’s report will update after every action is completed by the user.

* The following categories may be used to group the studies that have been claimed by the user:

* **Approval Queue**: This would contain the list of studies waiting to be approved by the user\(Trainee\) before the staff conducts final approval

* **Approved Today**: The list of the studies that have been approved by the trainee on a daily basis

* **Touched today**: This is the list of studies that have not been claimed but has been examined by the user.

* **Draft:** This is the list of the studies that have been claimed by the user but the user has neither chosen any template nor started any reporting on it. This would also have the status of Draft in the main central user interface.


**User Interface**

This is the interface where major activity on the status of the study would be displayed. It would be enabled to retrieve the list of the ordered study and also have capability of sorting of studies based on different criteria e.g. priority, date, type of study etc.

* **Reading List: This is the list of studies sent by the Technologist**

**Image Viewer**

This is an image viewing software used for the purpose of examining the batches of images in the study. It would be enabled with magnifying functionality.

| **Relationship Definition** |
| --- |

![](/assets/FLow chat.png)

**Preconditions**

* The Interface would have been enabled to retrieve patient information from the EMR system

* The dictation software would have been integrated with the reporting editor to enable the user to dictate observations.

* The template generator would have been design to retrieve metadata information from the DICOM images in the report.

* The image viewer would have been integrated to the user interface for image transfer

* The reporting software would have been enabled to communicate directly with the reading list of the ordered tests and the dictation software

* The RIS would have been designed to grant privileges based on roles of the user through recognition of login credentials \(See login Procedure page\)


**Task 1-Creating New Report**

Users should be able to create a new radiology report.

**Summary :** This task assists the users to create a report for a radiology study. The created report can be seen by a patient or referring doctor to help care for the patient . Various ways can be used to create a report - using voice dictation , templates or free-text typing.

**Users** : Must be radiologists \(trainees and staff\).

**Who cannot create reports**: Technologists , Referring doctors, PACS administrators

Steps

1. Login into the system \(See login Procedure page\)

2. View a reading list with studies that are yet to be dictated

3. Claim a study to start reporting:

  1. If the study is being reported on by someone else - Notify the user claiming the study and provide details of who is reporting this study

  2. If the study is already reported on and **finalized** - Inform user and ask if they would like to create an **addendum**

  3. If the study has **0 images** , confirm that the user would like to go ahead and make a report on this study

  4. If the study is in **Draft or Preliminary status** - then open the reporting interface to display the existing report



4.This action opens the text editor that supports reporting to be done \(It is similar to one ‘Composing a New Email’\) when claiming a report

The reporting editor should allow a user to create a report on the study:

* Should have a link  for templates available that can be used to generate the report

* Should support voice dictation when available

* Should provide a blank page that a user can type or pre-populate using template


Should support multimedia integration of key images from the study e.g if a user selects a specific instance from the study viewer , he\/she should be able to drag it to the editor to be incorporated into the report.

Once a user is happy with the report, then the user needs to exit the editor using the following options
1. 1. Can approve a report \(See Task 2\)

1. Can provide a preliminary report \(See Task 3\)

2. Can Draft a report \(See Task 4\)

3. Can Discard a report\( See Task-5\)


Outcome:

Verify that previous user\(Staff\) have to confirm relinquishing reporting authority to another user before user can resume reporting activing on a claimed study.

Verify that only a staff has the right to claimed any study that has been claimed before.

Verify that a staff may not claim a study from another staff without confirmation from the primary user\(staff\)

**Task 2- Approving Workflow for the Staff**

Users would be able to approve a study after completion of reporting activity.

**Summary**: The task of final approval is solely the responsibility of the user who is a staff. A staff goes through the list of the reports to perform different task on them, one of which is to change studies with “Preliminary” status to “Finalized” status after throrough examination of the reporting done by a trainee, at this point the report would be visible to both the patient and the ordering physician.

**Users **: Must be radiologists \(Staff only\).

**Who cannot approve a “preliminary” reports:** Trainees,Technologists , Referring Physicians, PACS Administrators

Steps

1. Login into the system \(See login Procedure page\)

2. View a reading list with studies with different status

3. right-click on a study to change the status \(See Task 1 for additional task for a staff-- Reporting\)

  1. Provide the user approving the study with details on who conducted reporting on the study

  2. For a **finalized** report- ask the user if they would like to create an **addendum**

  3. If the user decides to create **addendum, **open the reporting interface for the user to view an editable report \(See Task 1-Step 4\)

  4. if the user decides not to create **addendum, **open the reporting interface with broad report that is not editable



1. Once a user has added **addendum, **the user can exit the reporting interface.

Outcomes:

* Verify that the study attain 'Final' status regardless of the previous status

* Verify that a dialogue box ask if the user would like to finalize the report

* Verify that a user without a staff status would be prevented from finalizing a report

* verify that the report becomes visible to the ordering physician and the patient on the event that the user finalizes the report.


**Task 3- Approval Workflow for “Preliminary” Status**

A trainee can approve a report by changing the status from “Dictated” to “Preliminary”.

**Summary**: A user can only approve a study which has been previously claimed by the user, and the highest level of approval granted to a trainee is the”Preliminary”.Similarly, a user can claim a study in Draft or Dictated status reported on by another user provided that the primary user is not a staff. However, the saved report would be discarded for the current user to start reporting afresh.

Users : Must be radiologists \(Trainees and Staff\).

**Who cannot approve “Dictated” reports:** Technologists , Referring Physicians, PACS Administrators

Steps

1. Login into the system \(See login Procedure page\)

2. View a reading list with studies with different status

3. right-click on a study to change the status of the report:

4. Provide the user approving the study with details on who conducted reporting on the study

5. If the User login credentials is different from the credentials of the user who previously claimed the study, ask if the current user would like to reclaim the study.

6. If yes:\(See Task 4\)

7. If No: \(Notify the user to open another study\)

8. If the login credentials tallies with the current user’s login, ask the user if they would like to update the document:


1.            For a **Dictated** report- ask the user if they would like to create an **addendum**\(See Task 1-Step 4\).

2.            For a **Draft** report- ask the user if they would like to continue dictating the report\(See Task 1-Step 4\)

  Outcomes:


* Verify that a dialogue box ask if the user would like to approve for “Preliminary” status if the previous status is “Dictated”

* Verify that a dialogue box would ask if the user would like Save or approve to preliminary stage if the previous state is “Draft”


**Task 4- Workflow for a Draft**

Users should be able to claim a study and save as draft to continue reporting activity on it.

**Summary :** A user is allowed to claim more than one complete study for reporting. While working on a study, a user could save other studies that has been claimed as draft. A study in draft mode prevent other users to claim it.

**Users** : Must be radiologists \(trainees and staff\).

Who cannot create a draft: Technologists , Referring doctors, PACS administrators

Steps

1. Login into the system  \(See login Procedure page\)

2. View a reading list with studies that are yet to be dictated

3. Claim a study to start reporting \(See task 1\)

4. After claiming the study the user closes the editor\(empty\) and options of saving as draft or discard is given to the user


Outcome:

* Verify that a user can sort the lists with different criteria

* Verify that a dialogue box pops up to as “Would you like to save? Click yes to save as Draft, No to discard”

* Verify that the drafted study moves under the Draft folder in the user’s Reports

* Verify that the complete study goes back to the “unclaimed list” after the user discard


**Task 5- Workflow to Discard Report**

Users should be able to discard an already-claimed study regardless of its status. e.g“Preliminary”, “Draft” or even”Finalized”.

**Summary :** A user is allowed to discard a report if it seems unfit for diagnostic support. A staff should be able to discard any report regardless of who primarily claimed the study, while a trainee should only be able to discard a report that was initially claimed by the particular trainee.

**Users** : Must be radiologists \(trainees and staff\).

Who cannot create a draft: Technologists , Referring doctors, PACS administrators

Steps

1. Login into the system  \(See login Procedure page\)

2. View a reading list of all the studies

3. Right-click to view the study. For preliminary or finalized, a report will be available for view in document format. For draft, a blank report will be available.

4. A user may decide to discard the report by also right-clicking to choose “Discard the Report”

5. If the User is:


**a Staff:** The report can be discarded if the report was developed by any of the Trainees. The Trainee will be notified that their report has been discarded. A Staff may not discard a report developed by another staff.

**A Trainee:** The report can be discarded if the report was developed by the user. If developed by another user\(Trainee or Staff\), a dialogue box pops up to tell the user they are not authorized to perform the action.

Outcome:

* Verify that the trainee gets a notification that their report has been discarded

* Verify that the study status changes back to “completed”  After discarding it


**Task 6- ** **Collaboration Workflow for Communication between users**

Residents should have a way of communicating between themselves through a file-attachment enabled communication platform.

**Summary**: A user may require a second opinion in regards to a report done after a study examination. The user should be able to attached the report through a chatting platform that would be available to residents all the time. The recipient should be able to click on the file attached and view the report as long as it comes from the primary user who had previously claimed and reported on the complete study. The system would be designed to support drag-through capabilities.

Users : Must be radiologists \(Trainees and Staff\).

**Who cannot share “Draft or Preliminary” **Reports**:** Technologists, Referring Physicians, PACS Administrators

**Task 7-** **Template Generation **

