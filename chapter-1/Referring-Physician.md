# 1. Introduction

The following gives a general overview of how a Nurse Practitioner interacts with system on a daily basis. The focus is how the User places order to the radiology department, an action which is usually taken based on discretion rather than instruction by the Resident Physician.

## 2. Purpose

The purpose of the User Stories is to understand how the user prefers to interact with the order placement methodology basing our report on what currently exist and other things that are currently not existing but could make the order placement easier.

## 3. Intended user

The Intended user is a Nurse practitioner and also a Radiologist PA, who would have gone through the training as a Technologist. These individuals place order to the Technologist to conduct a certain procedure on a particular Patient.

## 4. Use Case

**Components of the System**

**Laboratory: **The laboratory tab should contain a spreadsheet categorized to see test from different procedures the patient has gone throughe.g Radiology, Biopsy, Ultrasound etc.

**Order: **This should contain a page where the user can place order to different section of the hospital.

**Review:** The section where the user can check the status of the ordered tests. The user should be able to  review new orders results as well as old ones.

**Task 1-Routine Check on Patient's status**

Users should be able to conduct a general routine check on a patient to determine if there might a reason to for further test.

**Summary :** Patient status cane be viewed through the laboratory section of the interface. The user can lookout for vital signs and make decisions such as ordering for aditional test.

**Users** : Must be Nurse Practitioner, Referring Physician.

Steps

1. Login into the system \(See login Procedure page\)

2. Search for the patient through their name or ID

3. View the labortatory tab to check for vital signs

4. if there is a need to conduct further check, place an order \(see task 2\)

Outcome:

**Task 2-Place an order **

Users  should can place an order to any of the sections of the healthcare ecosystem.

**Summary :** The user is allowed to place an order based on reports of the vital signs. The users can input information about the order in a form that has a dropdown contaning different procedures e.g Radiology, Ultrasound

**Users** : Must be Nurse Practitioner, Referring Physician.

Steps

1. The user is already logged in

2. Click the order tab

3. Choose the appropriate procedure type: which could be radiology, Biopsy e.t.c

4. The user clicks the selects the procedure title \(e.g. CT, US\) from the drop down. If it doesn’t exist, the user types it in the box.

5. The user writes a note in the **Additional Information** box to make the order clearer to the receipient\(s\), in this case a Radiologist e.g. P.O. Contrast

6. Go to to the box for **Reason for Examination.**  state the procedure in detail to as this is required to point the Technologist in the right direction and also to account for the accurate billing of the patient.

7. in the next box, set the priority to guide the technologist on how urgent the report is.

8. Send the order

Outcome:

Verify that the user gets notified to fill an empty box when they try to send an incomplete order

**4.1 Laboratory**

Under the Laboratory tab, the user sees a Vital Signs spreadsheet containing horizontally arranged tabs of different procedure sections of the Laboratory etc. Complete Blood Count\(CBC\).This is where the user derives the assertiveness to place any order based on the need of the patient.

**Task 3 Order Result Review**

User can view the list of ordered test under the review section.

**Summary :** The user clicks the tabs of different procedures to check the order result list. The result is expected to contained a finalized report from a Radiologist.

Steps

1. The user is already logged in

2. Click the Review tab

3. click the Radiology column to view the list of the reports

4. click the report to view the breakdown\(see the Resident page for the format of the report\)

Outcome:

Verify that the user can sort the list of the report based on differrent criteria e.g Day, time\).

Verify that the newer reports are at the top of the list.

Verify that the new report are in deep colors to differentiate from older ones.

