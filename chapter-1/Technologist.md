# The following describes the concern of the Technologist when interacting with the System:

List of actions expected from a Technologist:

| **Navigation Through the Workspace** |
| --- |
| Preconditions:  Privileges would have been created based on rank of the Technologists. A Team Lead gets to have access to more Apps om the dashboard as shown below. Roles of a Lead Technologist include granting privileges to Junior Technologists as well as  other data tracking activities. |

Flow of Events:

The  Technologist would like to have a moveable dashboard containing Icons for easy navigation to the sections of applications such as:

* Woklist: 
* Department Order entry: 
* Appointment Scheduling
* Analytics
* Analytics Administration
* Order View
* Order Removal

| **Worklist** |
| --- |
| A Table which contains all the color-coded worklists from all the affiliated hospitals. This makes it easy for the Technologist to prioritize appointment scheduling |

![](/assets/Worklist.png)





