| **Logging into the System** |
| --- |

Users have different privileges based on their position. While some users are expected to have more privileges  than others, some would also be restricted from performing certain actions.

**Summary**: The actions that a user is allowed to perform will be based on the type of privileges granted with consideration for the designated task. Also, the level of the user in the hierachy is considered before grating privileges.

**Who cannot approve a “preliminary” reports:** Trainees,Technologists , Referring Physicians, PACS Administrators

Steps

1. A user is required to  punch in userId and passcode, which would have been given by an administrator.

2. The userid can be designed to have prefixes match the category of user e.g TCH0123, RS012, RT012,

3. at the point of logging in, the systems is designed to recognize the category of user and grant privileges based on their category.If the user is:


* **Resident:** The user is granted reporting privileges such as claiming a study, editing a study, updating a study.
  **If the Resident is a Staff**: The user is granted all privileges including:

* Claiming a study in all  stutus, provided it has not been currently claimed by a staff.

* Drafting a study

* Approving a report except the ones being currently conducted by another staff

* Adding addendum to  any report other than a report currently worked on by another staff

* Discarding any report other than report currently worked on by another staff


 **     If the Resident is a Trainee**: The user is granted privileges including:

* Claiming a study in a 'completed' status
* Drafting a claimed study for later reporting activities
* Updating a report from a 'draft' status to a 'preliminary' status
* Adding addendum to a report that has only being claimed by the current user
* Communicate with other user over the communication channel, including file exchange etc.

* **Technologist:**  The user is granted privileges such as uploading studies for the reporting activities of residents.


**Outcome**s:

* Verify that A staff gets a popup  when they try to overide priviledgs of other Staff. e.g "You are not authorize to overide another Staff's priviledges"

* Verify that a trainee gets prevented from overiding priviledges of other users.


