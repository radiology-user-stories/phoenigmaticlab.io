# Summary

* [Introduction](README.md)
* [Chapter One](.md)
* [Table of Content](chapter-1/TableOfContent.md)
* [Login Procedure](logging-in-procedure.md)
* [Residents](chapter-1/Residents.md)
* [Technologists](chapter-1/Technologist.md)
* [PACS Manager](chapter-1/PACS-Manager.md)
* [Nurse Practitioner](chapter-1/Referring-Physician.md)

